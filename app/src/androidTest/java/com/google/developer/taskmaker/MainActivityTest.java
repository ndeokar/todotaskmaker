package com.google.developer.taskmaker;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityUnitTestCase;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.allOf;

/**
 * Created by Niel on 19/02/17.
 */


@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest extends ActivityUnitTestCase<MainActivity> {

  public MainActivityTest() {
    super(MainActivity.class);
  }

  @Rule
  public ActivityTestRule<MainActivity> menuActivityTestRule =
      new ActivityTestRule<>(MainActivity.class, true, true);


  @Test
  public void testClickFloatinButton_opensAddTaskScreen() {
    //locate and click on the fab button
    onView(withId(R.id.fab)).perform(click());

    //check if the add task screen is displayed by asserting that the description edittext is displayed
    onView(withId(R.id.text_input_description)).check(matches(allOf(isDescendantOfA(withId(R.id.layout_add_task )), isDisplayed())));
  }


}
