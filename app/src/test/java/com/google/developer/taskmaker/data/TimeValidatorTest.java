package com.google.developer.taskmaker.data;

import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.internal.JUnitSystem;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


@RunWith(JUnit4.class)
public class TimeValidatorTest {

    @Test
    public void timeValidator_testHourToMsConversion() {

        assertEquals("Conversion to MS Sucess",  3600000L, Time.convertHourToMS(1));

        assertEquals("Conversion to Hour Sucess",  1, Time.convertMStoHour(3600000L));

    }

}