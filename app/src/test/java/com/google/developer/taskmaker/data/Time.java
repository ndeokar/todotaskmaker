package com.google.developer.taskmaker.data;

import java.util.concurrent.TimeUnit;

/**
 * Created by Niel on 19/02/17.
 */

public class Time {
    public static long convertMStoHour(long ms) {
      return TimeUnit.MILLISECONDS.toHours(ms);
    }

  public static long convertHourToMS(long hour) {
    return TimeUnit.HOURS.toMillis(hour);
  }
}
