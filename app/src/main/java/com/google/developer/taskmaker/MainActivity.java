package com.google.developer.taskmaker;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.icu.text.LocaleDisplayNames;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.google.developer.taskmaker.data.DatabaseContract;
import com.google.developer.taskmaker.data.TaskAdapter;
import com.google.developer.taskmaker.data.TaskUpdateService;
import com.google.developer.taskmaker.view.TaskTitleView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.developer.taskmaker.data.DatabaseContract.CONTENT_URI;

public class MainActivity extends AppCompatActivity implements
        TaskAdapter.OnItemClickListener,
        View.OnClickListener,
    LoaderManager.LoaderCallbacks<Cursor>{

    private TaskAdapter mAdapter;

    private static final int LOADER_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdapter = new TaskAdapter(null);
        mAdapter.setOnItemClickListener(this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // initialise loader
        getSupportLoaderManager().initLoader(LOADER_ID, null, this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /* Click events in Floating Action Button */
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, AddTaskActivity.class);
        startActivity(intent);
    }

    /* Click events in RecyclerView items */
    @Override
    public void onItemClick(View v,  int position) {
        Intent intent = new Intent(this, TaskDetailActivity.class);
        intent.setData(Uri.parse(CONTENT_URI+ "/" + mAdapter.getItemId(position)));

        List<Pair<View, String>> pairs = new ArrayList<>();

        pairs.add(Pair.create(v.findViewById(R.id.text_description), getString(R.string.transition_desc)));
        pairs.add(Pair.create(v.findViewById(R.id.priority), getString(R.string.transition_priority)));

        if(mAdapter.getItem(position).hasDueDate()){
            pairs.add(Pair.create(v.findViewById(R.id.text_date), getString(R.string.transition_date)));
        }

        Pair<View, String>[] finalPairs = createSafeTransitionParticipants(MainActivity.this, false,
            pairs.toArray(new Pair[pairs.size()]));


        ActivityOptionsCompat options = ActivityOptionsCompat.
            makeSceneTransitionAnimation(this,
                finalPairs);
        startActivity(intent, options.toBundle());
        //startActivity(intent);
    }



    public static Pair<View, String>[] createSafeTransitionParticipants(@NonNull Activity activity,
        boolean includeStatusBar, @Nullable Pair... otherParticipants) {
        // Avoid system UI glitches as described here:
        // https://plus.google.com/+AlexLockwood/posts/RPtwZ5nNebb
        View decor = activity.getWindow().getDecorView();
        View statusBar = null;
        if (includeStatusBar) {
            statusBar = decor.findViewById(android.R.id.statusBarBackground);
        }
        View navBar = decor.findViewById(android.R.id.navigationBarBackground);

        // Create pair of transition participants.
        List<Pair> participants = new ArrayList<>(3);
        addNonNullViewToTransitionParticipants(statusBar, participants);
        addNonNullViewToTransitionParticipants(navBar, participants);
        // only add transition participants if there's at least one none-null element
        if (otherParticipants != null && !(otherParticipants.length == 1
            && otherParticipants[0] == null)) {
            participants.addAll(Arrays.asList(otherParticipants));
        }
        return participants.toArray(new Pair[participants.size()]);
    }

    private static void addNonNullViewToTransitionParticipants(View view, List<Pair> participants) {
        if (view == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            participants.add(new Pair<>(view, view.getTransitionName()));
        }
    }

    /* Click events on RecyclerView item checkboxes */
    @Override
    public void onItemToggled(boolean active, int position) {
        ContentValues values = new ContentValues(1);
        values.put(DatabaseContract.TaskColumns.IS_COMPLETE, active ? TaskTitleView.DONE  : TaskTitleView.NORMAL);
        Uri uri = Uri.parse(CONTENT_URI+ "/" + mAdapter.getItemId(position));
        TaskUpdateService.updateTask(this,uri,values);
    }

    @Override public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String strSortType = sp.getString(getString(R.string.pref_sortBy_key), "NA");

        String sordOrder = strSortType.equalsIgnoreCase(getString(R.string.pref_sortBy_due)) ?  DatabaseContract.DATE_SORT : DatabaseContract.DEFAULT_SORT ;

        return new CursorLoader(MainActivity.this, CONTENT_URI,
            null, null, null, sordOrder);
    }

    @Override public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        switch (loader.getId()) {
            case LOADER_ID:
                mAdapter.swapCursor(data);
                break;
        }
    }

    @Override public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }
}
