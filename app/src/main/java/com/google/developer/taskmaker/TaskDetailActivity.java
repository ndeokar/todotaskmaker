package com.google.developer.taskmaker;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.developer.taskmaker.data.DatabaseContract;
import com.google.developer.taskmaker.data.Task;
import com.google.developer.taskmaker.data.TaskUpdateService;
import com.google.developer.taskmaker.reminders.AlarmScheduler;
import com.google.developer.taskmaker.view.DatePickerFragment;
import com.google.developer.taskmaker.view.TaskTitleView;
import java.util.Calendar;

public class TaskDetailActivity extends AppCompatActivity
    implements DatePickerDialog.OnDateSetListener {

  private Task mTask;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_task_detail);
    //Task must be passed to this activity as a valid provider Uri
    final Uri taskUri = getIntent().getData();

    Cursor cursor = getContentResolver().query(taskUri, null, null, null, null);

    if (cursor != null && cursor.moveToFirst()) {
      mTask = new Task(cursor);

      TaskTitleView nameView = (TaskTitleView) findViewById(R.id.text_description);
      TextView dateView = (TextView) findViewById(R.id.text_date);
      ImageView priorityView = (ImageView) findViewById(R.id.priority);

      int mState = mTask.dueDateMillis < (System.currentTimeMillis()- 1000) ? TaskTitleView.OVERDUE : TaskTitleView.NORMAL  ;

      if(mTask.isComplete ){
        mState = TaskTitleView.DONE;
      }

      nameView.setState(mState);

      nameView.setText(mTask.description);

      CharSequence formattedDate = mTask.hasDueDate() ? DateFormat.format("MM/dd/yyyy", mTask.dueDateMillis) : getString(R.string.date_empty)  ;
      dateView.setText(formattedDate);

      if (mTask.isPriority) {
        priorityView.setImageResource(R.drawable.ic_priority);
      } else {
        priorityView.setImageResource(R.drawable.ic_not_priority);
      }
    }
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_task_detail, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {
      case R.id.action_reminder:
        DatePickerFragment dialogFragment = new DatePickerFragment();
        dialogFragment.show(getSupportFragmentManager(), "datePicker");
        return true;
      case R.id.action_delete:
        deleteItem();
        return true;
      case android.R.id.home:
        supportFinishAfterTransition();
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private void deleteItem() {
    Uri uri = Uri.parse(DatabaseContract.CONTENT_URI + "/" + mTask.id);
    TaskUpdateService.deleteTask(this, uri);
    finish();
  }

  @Override public void onDateSet(DatePicker view, int year, int month, int day) {

    final long today = System.currentTimeMillis() - 1000;

    Calendar calendar = Calendar.getInstance();
    calendar.set(year, month, day);
    //If user tries to select date in past (or today)
    if (calendar.getTimeInMillis() < today) {
      //Make them try again
      makeToast("Past date is not allowed");
      return;
    }
    //Set to noon on the selected day
    Calendar c = Calendar.getInstance();
    c.set(Calendar.YEAR, year);
    c.set(Calendar.MONTH, month);
    c.set(Calendar.DAY_OF_MONTH, day);
    c.set(Calendar.HOUR_OF_DAY, 12);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);

    Uri uri = Uri.parse(DatabaseContract.CONTENT_URI + "/" + mTask.id);
    AlarmScheduler.scheduleAlarm(this, c.getTimeInMillis(),uri);
  }

  private void makeToast(String mgs) {
    Toast.makeText(TaskDetailActivity.this, mgs, Toast.LENGTH_LONG).show();
  }
}
